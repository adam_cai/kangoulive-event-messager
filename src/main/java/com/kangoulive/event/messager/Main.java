package com.kangoulive.event.messager;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;

public final class Main {

	private Main() {
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(Application.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

}
