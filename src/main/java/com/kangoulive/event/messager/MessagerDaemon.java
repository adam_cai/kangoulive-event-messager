package com.kangoulive.event.messager;

import org.apache.commons.daemon.DaemonContext;
import org.oxerr.spring.boot.daemon.SpringApplicationDaemon;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;

public class MessagerDaemon extends SpringApplicationDaemon {

	@Override
	protected SpringApplication initSpringApplication(DaemonContext context) {
		SpringApplication app = new SpringApplication(Application.class);
		app.setBannerMode(Banner.Mode.OFF);
		return app;
	}

}
