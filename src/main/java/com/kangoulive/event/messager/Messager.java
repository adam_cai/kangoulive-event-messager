package com.kangoulive.event.messager;

import java.io.IOException;
import java.time.Instant;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kangoulive.client.KangouLiveClient;
import com.kangoulive.domain.event.EnrollStatus;
import com.kangoulive.domain.event.Event;
import com.kangoulive.domain.event.EventTicket;

@Component
public class Messager {

	private final Logger log = LogManager.getLogger();

	private final ObjectMapper objectMapper;
	private final KangouLiveClient client;

	@Autowired
	public Messager(ObjectMapper objectMapper, KangouLiveClient client) {
		this.objectMapper = objectMapper;
		this.client = client;
	}

	@JmsListener(destination = "${com.kangoulive.event.message-queue-name}", concurrency = "1")
	public void handleMessage(String message) throws IOException {
		log.debug("Consuming: {}", message);
		Event event = this.objectMapper.readValue(message, Event.class);
		log.debug("event: {}", () -> ToStringBuilder.reflectionToString(event));
		handleMessage(event);
		log.debug("Consumed: {}", message);
	}

	@JmsListener(destination = "${com.kangoulive.event.balance-queue-name}", concurrency = "1")
	public void handleBalanceMessage(String message) throws IOException {
		log.debug("Consuming: {}", message);
		Event event = this.objectMapper.readValue(message, Event.class);
		log.debug("event: {}", () -> ToStringBuilder.reflectionToString(event));
		handleBalanceMessage(event);
		log.debug("Consumed: {}", message);
	}
	
	@JmsListener(destination = "${com.kangoulive.event.refund-queue-name}", concurrency = "1")
	public void handleRefundMessage(String message) throws IOException {
		log.debug("Consuming: {}", message);
		EventTicket eventTicket = this.objectMapper.readValue(message, EventTicket.class);
		log.debug("eventTicket: {}", () -> ToStringBuilder.reflectionToString(eventTicket));
		handleRefundMessage(eventTicket);
		log.debug("Consumed: {}", message);
	}

	private void handleRefundMessage(EventTicket eventTicket) {
		EventTicket freshEventTicket = this.client.getFailedEventTicketRefundService().getEventTicket(eventTicket.getId());
		log.debug("freshEventTicket: {}", () -> ToStringBuilder.reflectionToString(freshEventTicket));
		if(
			eventTicket.getStatus().equals(EnrollStatus.PAID_OVER) || 
			eventTicket.getStatus().equals(EnrollStatus.PAID_FAIL)
		) {
			refund(freshEventTicket);
		}
	}

	private void handleMessage(final Event event) {
		Event freshEvent = this.client.getEventService().getEvent(event.getId());
		log.debug("freshEvent: {}", () -> ToStringBuilder.reflectionToString(freshEvent));

		// freshEvent.isAfterReminderingTime();

		if (freshEvent.isAfterReminderingTime()) {
			notify(freshEvent);
		} else {
			throw new IllegalStateException("Current time is not after remindering time.");
		}
	}

	private void handleBalanceMessage(Event event) {
		Event freshEvent = this.client.getEventService().getEvent(event.getId());
		log.debug("freshEvent: {}", () -> ToStringBuilder.reflectionToString(freshEvent));

		if (!freshEvent.isBalanced() && Instant.now().isAfter(freshEvent.getDeadline())) {
			balance(freshEvent);
		}
	}

	private void balance(Event freshEvent) {
		this.client.getEventService().balance(freshEvent.getId());
	}

	private void notify(Event event) {
		this.client.getEventService().reminder(event.getId());
	}
	
	private void refund(EventTicket freshEventTicket) {
		this.client.getFailedEventTicketRefundService().refund(freshEventTicket.getId());
	}


}
