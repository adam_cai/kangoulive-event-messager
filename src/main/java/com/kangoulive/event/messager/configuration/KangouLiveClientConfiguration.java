package com.kangoulive.event.messager.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kangoulive.client.KangouLiveClient;
import com.kangoulive.client.KangouLiveClientProperties;

@Configuration
public class KangouLiveClientConfiguration {

	@Bean
	public KangouLiveClientProperties kangouLiveClientProperties() {
		return new KangouLiveClientProperties();
	}

	@Bean
	@Autowired
	public KangouLiveClient kangouLiveClient(KangouLiveClientProperties kangouLiveClientProperties) {
		return new KangouLiveClient(
			kangouLiveClientProperties.getBaseUri(),
			kangouLiveClientProperties.getUsername(),
			kangouLiveClientProperties.getPassword()
		);
	}

}
