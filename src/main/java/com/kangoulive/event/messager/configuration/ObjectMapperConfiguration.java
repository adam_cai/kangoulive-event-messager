package com.kangoulive.event.messager.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kangoulive.jackson.KangouLiveObjectMapperBuilder;

@Configuration
public class ObjectMapperConfiguration {

	@Bean
	public ObjectMapper objectMapper() {
		return new KangouLiveObjectMapperBuilder().build();
	}

}
